def fibonaccirecursivo(n):

    if n==0:
        return 0
    if n==1:
        return 1
    return fibonaccirecursivo(n-1) + fibonaccirecursivo(n-2)

n = int(input("Serie Fibonacci hasta el numero: "))

for i in range(0,n):
    print("Fibonacci de: " + str(i+1) + " es " + str(fibonaccirecursivo(i)))